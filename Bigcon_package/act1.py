# TensorFlow를 이용하여 선형회귀 모형으로 구별한다
# 각 데이터의 상관관계를 찾아서 알맞는 데이터를 구한다
'''
상관 분석 - 위키백과
피어슨 상관 계수(Pearson correlation coefficient 또는 Pearson's r)는 두 변수간의 관련성을 구하기 위해 보편적으로 이용된다.
r이 -1.0과 -0.7 사이이면, 강한 음적 선형관계
r이 -0.7과 -0.3 사이이면, 뚜렷한 음적 선형관계
r이 -0.3과 -0.1 사이이면, 약한 음적 선형관계
r이 -0.1과 +0.1 사이이면, 거의 무시될 수 있는 선형관계
r이 +0.1과 +0.3 사이이면, 약한 양적 선형관계
r이 +0.3과 +0.7 사이이면, 뚜렷한 양적 선형관계
r이 +0.7과 +1.0 사이이면, 강한 양적 선형관계
'''
# 즉 절대값 0.3이상의 데이터는 유의미한 데이터로 취급할것이며, 0.3 이하의 데이터는 zero에서 시작하게 설정한다
# 각 결과값에 대한 인풋 파일을 결정짖는 것은 절대값 0.5 이상의 데이터의 수가 많은 순서 혹은 절반이상이 유의미한 데이터일 경우이다
# 모든 해당사항에 맞지 않는경우 모든 값으로 결과를 시각화하여 유의미한 데이터를 구별한다
# ---------------------------------------------------------------------------------------------

import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np


result_csv = train_label.csv
csv_list =['train_activity.csv', 'train_combat.csv', 'train_pledge.csv', 'train_trade.csv', 'train_payment.csv']
result = np.loadtxt('', delimiter=',', dtype=np.float32)       #id = result[:,[0]], 생존일,결제금 = reslut[:,1:]

def tensor_Lin(inputX, inputY):
    tf.reset_default_graph()
    r = tf.random_uniform([1], -1.0, 1.0)
    W = tf.Variable(r, name="W")
    b = tf.Variable(tf.zeros([1]), name="Bias")
    x = tf.placeholder(tf.float32, name="X")
    y = tf.placeholder(tf.float32, name="Y")

    # Loss function을 정의한다. (MSE : Mean Square Error)
    predY = tf.add(tf.multiply(W, x), b)
    loss = tf.reduce_mean(tf.square(predY - y))

    # 학습할 optimizer를 정의한다
    optimizer = tf.train.AdamOptimizer(0.05)
    train = optimizer.minimize(loss)

    # 세션을 오픈하고 그래프를 실행한다.
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        trLoss = []
        for i in range(100):
            _, resultLoss = sess.run([train, loss], feed_dict={x: inputX, y: inputY})
        trLoss.append(resultLoss)

        if i % 10 == 0:
            print("%d) %f" % (i, resultLoss))

        sW = sess.run(W)
        sb = sess.run(b)

        # 결과를 확인한다
        print("\n*회귀직선의 방정식 (OLS) : y = %.4f * x +  %.4f" % (sW, sb))
        y = sW * inputX + sb

        fig = plt.figure(figsize=(10, 4))
        p1 = fig.add_subplot(1, 2, 1)
        p2 = fig.add_subplot(1, 2, 2)

        p1.plot(inputX, inputY, 'ro', markersize=1.5)
        p1.plot(inputX, y)

        p2.plot(trLoss, color='red', linewidth=1)
        p2.set_title("Loss function")
        p2.set_xlabel("epoch")
        p2.set_ylabel("loss")
        plt.show()

#X를 넣어야한다
tensor_Lin(inputX, reslut[:,[1]])